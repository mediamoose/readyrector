from django.test.testcases import TestCase

from readyrector import forms, models


class RedirectFormTestCase(TestCase):
    def test_parse_from_path(self):
        data = {
            "from_path": "test/",
            "to_url": "https://www.mediamoose.nl/",
        }
        form = forms.RedirectForm(data=data)
        assert form.is_valid()
        assert form.cleaned_data.get("from_path") == "test/"

        data = {
            "from_path": "/test/",
            "to_url": "https://www.mediamoose.nl/",
        }
        form = forms.RedirectForm(data=data)
        assert form.is_valid()
        assert form.cleaned_data.get("from_path") == "test/"

        data = {
            "from_path": "/test/?lorem=ipsum",
            "to_url": "https://www.mediamoose.nl/",
        }
        form = forms.RedirectForm(data=data)
        assert form.is_valid()
        assert form.cleaned_data.get("from_path") == "test/"

        data = {
            "from_path": "/ ?",
            "to_url": "https://www.mediamoose.nl/",
        }
        form = forms.RedirectForm(data=data)
        assert not form.is_valid()
        assert form.has_error("from_path", "invalid")

    def test_from_path_unique(self):
        data = {
            "from_path": "test/",
            "to_url": "https://www.mediamoose.nl/",
        }
        form = forms.RedirectForm(data=data)
        assert form.is_valid(), form.errors.as_json()

        redirect = models.Redirect.objects.create(
            from_path="test/", to_url="https://www.mediamoose.nl/"
        )
        data = {
            "from_path": "test/",
            "to_url": "https://www.mediamoose.nl/",
        }
        form = forms.RedirectForm(data=data)
        assert not form.is_valid()
        assert form.has_error("from_path", "from_path_not_unique")

        form = forms.RedirectForm(data=data, instance=redirect)
        assert form.is_valid(), form.errors.as_json()

    def test_save(self):
        redirect = models.Redirect.objects.create(
            from_path="test/", to_url="https://www.mediamoose.nl/", hits=10
        )
        data = {
            "from_path": "test/",
            "to_url": "https://www.mediamoose.nl/",
        }
        form = forms.RedirectForm(instance=redirect, data=data)
        assert form.is_valid()
        instance = form.save(commit=False)
        assert instance.hits == 10

        data = {
            "from_path": "testen/",
            "to_url": "https://www.mediamoose.nl/",
        }
        form = forms.RedirectForm(instance=redirect, data=data)
        assert form.is_valid()
        instance = form.save(commit=False)
        assert instance.hits == 0

        instance.refresh_from_db()
        assert instance.hits == 10

        form = forms.RedirectForm(instance=redirect, data=data)
        assert form.is_valid()
        instance = form.save(commit=True)
        assert instance.hits == 0

        instance.refresh_from_db()
        assert instance.hits == 0
