import os

from django.core.exceptions import ValidationError
from django.test import SimpleTestCase

from readyrector.validators import PathOrURLValidator


path_or_url_validator = PathOrURLValidator()


def create_path(filename):
    return os.path.abspath(os.path.join(os.path.dirname(__file__), filename))


class RedirectFormTestCase(SimpleTestCase):
    def test_valid_urls(self):
        with open(create_path("valid_urls.txt"), encoding="utf8") as f:
            for url in f:
                assert path_or_url_validator(url.strip()) is None

    def test_invalid_urls(self):
        with self.assertRaises(ValidationError):
            path_or_url_validator("")

        with open(create_path("invalid_urls.txt"), encoding="utf8") as f:
            for url in f:
                with self.assertRaises(ValidationError):
                    path_or_url_validator(url.strip())

    def test_schemes(self):
        with self.assertRaises(ValidationError):
            PathOrURLValidator(schemes=("https",))("http://www.mediamoose.nl/")

        assert (
            PathOrURLValidator(schemes=("http",))("http://www.mediamoose.nl/") is None
        )
