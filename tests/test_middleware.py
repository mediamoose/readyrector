from django.core.exceptions import ImproperlyConfigured
from django.test.testcases import TestCase
from django.test.utils import override_settings

from readyrector import models


@override_settings(MIDDLEWARE=("readyrector.middleware.RedirectDatabaseMiddleware",))
class RedirectDatabaseMiddlewareTestCase(TestCase):
    def test_existing_path(self):
        resp = self.client.get("/exists/")
        assert resp.status_code == 200

        redirect = models.Redirect.objects.create(
            from_path="exists/", to_url="https://www.mediamoose.nl/"
        )
        assert redirect.hits == 0

        resp = self.client.get("/exists/")
        assert resp.status_code == 200

        redirect.refresh_from_db()
        assert redirect.hits == 0

    def test_non_existing_path(self):
        resp = self.client.get("/not-found/")
        assert resp.status_code == 404

        redirect = models.Redirect.objects.create(
            from_path="not-found/", to_url="https://www.mediamoose.nl/"
        )
        assert redirect.hits == 0

        resp = self.client.get("/not-found/")
        assert resp.status_code == 302
        self.assertRedirects(
            resp, "https://www.mediamoose.nl/", 302, fetch_redirect_response=False
        )

        redirect.refresh_from_db()
        assert redirect.hits == 1

        resp = self.client.get("/not-found")
        assert resp.status_code == 302
        self.assertRedirects(
            resp, "https://www.mediamoose.nl/", 302, fetch_redirect_response=False
        )


@override_settings(MIDDLEWARE=("readyrector.middleware.RedirectURLConfMiddleware",))
class RedirectURLConfMiddlewareTestCase(TestCase):
    @override_settings(READYRECTOR_URLCONF="")
    def test_required_setting(self):
        with self.assertRaises(ImproperlyConfigured):
            resp = self.client.get("/not-found/")
            assert resp.status_code == 200

    def test_existing_path(self):
        resp = self.client.get("/exists/")
        assert resp.status_code == 200

    def test_redirect_path(self):
        resp = self.client.get("/redirect/")
        assert resp.status_code == 302
        self.assertRedirects(
            resp, "https://www.mediamoose.nl/", 302, fetch_redirect_response=False
        )

        resp = self.client.get("/redirect")
        assert resp.status_code == 302
        self.assertRedirects(
            resp, "https://www.mediamoose.nl/", 302, fetch_redirect_response=False
        )

        with override_settings(APPEND_SLASH=False):
            resp = self.client.get("/redirect")
            assert resp.status_code == 404

    def test_non_existing(self):
        resp = self.client.get("/not-found/")
        assert resp.status_code == 404

        resp = self.client.get("/not-found")
        assert resp.status_code == 404
