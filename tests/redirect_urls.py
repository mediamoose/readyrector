from django.conf.urls import url
from django.views.generic import RedirectView


urlpatterns = [
    url(r"^exists/$", RedirectView.as_view(url="https://www.mediamoose.nl/")),
    url(r"^redirect/$", RedirectView.as_view(url="https://www.mediamoose.nl/")),
]
